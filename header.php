
<!doctype html>
<html class="no-js" lang="en">

<!-- Mirrored from html.xpeedstudio.com/trypto/index-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 11 Jan 2020 13:54:42 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>CRYPTOSKY</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

   <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i,800,800i" rel="stylesheet">


   <link rel="icon" type="image/png" href="favicon.html">
   <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="apple-touch-icon.html">

    <link rel="stylesheet" href="assets/css/fontawesome-min.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/xsIcon.css">
    <link rel="stylesheet" href="assets/fonts/icomoon/style.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="assets/css/navigation.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/1.css">

    <!--Theme custom css -->
    <link rel="stylesheet" href="assets/css/style.css">

    <!--Theme Responsive css-->
    <link rel="stylesheet" href="assets/css/responsive.css"/>

    <!-- use only color version -->
    <!-- <link rel='stylesheet' type='text/css' href='assets/css/colors/color-1.css' > -->
    <style type="text/css">
        .preloader{
           background-image: linear-gradient(to left, #027762 0%, #026054 47%, #014945 99%);
        }
    </style>
</head>
<body>
<!--[if lt IE 10]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<!-- preloader  -->

<!--     <div class="preloader" id="preloader">
        <svg class="xs-preload" viewBox="0 0 120 120" width="120px" height="120px">
            <circle class="inner" cx="60" cy="60" r="32"/>
            <circle class="middle" cx="60" cy="60" r="38"/>
            <circle class="outer" cx="60" cy="60" r="44"/>
        </svg>
         <a class="cencel-preloader" href="#">Cancel Preloader</a>
    </div> -->

    <!-- preloader end  -->

<div class="search-wrap">
    <form class="search-inner" id="main_search" method="get">
        <input id="search_text" type="search" placeholder="Search..." autocomplete="off">
        <button id="searchbtn" type="submit"><i class="fa fa-search"></i></button>
    </form>
</div>






<!-- offset cart strart -->
<!-- sidebar cart item -->
<div class="xs-sidebar-group cart-group">
    <div class="xs-overlay xs-bg-black"></div>
    <div class="xs-sidebar-widget">
        <div class="sidebar-widget-container">
            <div class="widget-heading media">
                <div class="media-body">
                    <a href="#" class="close-side-widget">
                        <i class="icon icon-cross"></i>
                    </a>
                </div>
            </div>
            <div class="xs-empty-content">
                <h3 class="widget-title">Shopping cart</h3>
                <h4 class="xs-title">No products in the cart.</h4>
                <p class="empty-cart-icon">
                    <i class="icon icon-shopping-cart"></i>
                </p>
                <p class="xs-btn-wraper">
                    <a class="btn btn-primary" href="#">Return To Shop</a>
                </p>
            </div>
        </div>
    </div>
</div>    <!-- END sidebar cart item -->    <!-- END offset cart strart -->


<!-- sidebar cart item -->
<div class="xs-sidebar-group info-group">
    <div class="xs-overlay xs-bg-black"></div>
    <div class="xs-sidebar-widget">
        <div class="sidebar-widget-container">
            <div class="widget-heading">
                <a href="#" class="close-side-widget">
                    <i class="icon icon-cross"></i>
                </a>
            </div>
            <div class="sidebar-textwidget">
                <div class="sidebar-logo-wraper">
                    <img src="assets/images/sidebar_logo_2.png" alt="sidebar logo">
                </div>
                <p></p>
                <ul class="sideabr-list-widget">
                    <li>
                        <div class="media">
                            <div class="d-flex">
                                <i class="icon icon-placeholder2"></i>
                            </div>
                            <div class="media-body">
                                <p>32 London Bridge St 12</p>
                                <span>London, England</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                    <li>
                        <div class="media">
                            <div class="d-flex">
                            <i class="icon icon-email2"></i>
                            </div>
                            <div class="media-body">
                                <a href="mailto:info@domain.com">info@cryptogemx.com</a>
                                <span>Online Support</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                    <li>
                        <div class="media">
                            <div class="d-flex">
                            <i class="icon icon-phone-call2"></i>
                            </div>
                            <div class="media-body">
                                <p>+447555647840</p>
                                <span>Mon-Fri 8am-5pm</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                </ul><!-- .sideabr-list-widget -->
                <div class="subscribe-from">
                    <form action="#" method="POST" id="subscribe-form" class="subscribe-form">
                        <label for="sub-input"></label>
                        <div class="form-group">
                            <input type="email" name="email" id="sub-input" placeholder="Enter your mail here" class="form-control">
                            <button class="sub-btn" type="submit"><i class="icon icon-arrow-right"></i></button>
                        </div>
                    </form>
                </div>
                <ul class="social-list version-2">
                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#" class="vimeo"><i class="fa fa-vimeo"></i></a></li>
                </ul><!-- .social-list -->
                <div class="text-center">
                    <a href="#" class="btn btn-primary">Get Subscribed!</a>
                </div>
            </div>
        </div>
    </div>
</div>    <!-- END sidebar widget item -->    <!-- END offset cart strart -->
    <!-- header section start -->
    <header class="header header-v3">
    <div class="container">
        <nav id="navigation1" class="navigation">

            <div class="nav-header">
                <a class="nav-brand" href="https://cryptogemx.com/">
                    <img src="assets/images/trypto_logo_green.png" alt="">
                </a>
                <div class="nav-toggle"></div>
            </div>

            <div class="nav-menus-wrapper xs-menu">
                <ul class="header-right align-to-right">
                <!-- <li class="language">
                        <a href="#modal-popup-1" class="xs-modal-popup">
                            <img src="assets/images/english.png" alt="">
                            EN
                        </a>
                            <div class="zoom-anim-dialog mfp-hide modal-language" id="modal-popup-1">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="language-content">
                                            <p>Switch The Language</p>
                                            <ul class="flag-lists">
                                                <li><a href="#"><img src="assets/images/flags/006-united-states.svg" alt=""><span>English</span></a></li>
                                                <li><a href="#"><img src="assets/images/flags/002-canada.svg" alt=""><span>English</span></a></li>
                                                <li><a href="#"><img src="assets/images/flags/003-vietnam.svg" alt=""><span>Vietnamese</span></a></li>
                                                <li><a href="#"><img src="assets/images/flags/004-france.svg" alt=""><span>French</span></a></li>
                                                <li><a href="#"><img src="assets/images/flags/005-germany.svg" alt=""><span>German</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </li> -->
                    <li>
                        <a href="https://client.cryptogemx.com/login" class="logincustom">
                            My Account
                        </a>
                    </li>

                    <!-- <li>
                        <a href="https://client.cryptogemx.com/register" class="registercustom">
                            Register
                        </a>
                    </li> -->

                    <li>
                        <a href="#" class="navSidebar-button">
                            <i class="icon-menu2"></i>
                        </a>
                    </li>
                </ul>
                <ul class="nav-menu align-to-right">
                    <li class="active"><a href="https://cryptogemx.com/">Home</a>
                        <!-- <div class="megamenu-panel">
                            <div class="megamenu-panel-row">
                                <div class="col-lg-4 col-12">
                                    <a href="index.html">
                                        <img style="width: 100%" src="assets/images/menu_img/home_1.jpg" alt="image">
                                        <span>Home One</span>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-12">
                                    <a href="index-2.html">
                                        <img style="width: 100%" src="assets/images/menu_img/home_2.jpg" alt="image">
                                        <span>Home Two</span>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-12">
                                    <a href="index-3.html">
                                            <img style="width: 100%" src="assets/images/menu_img/home_3.jpg" alt="image">
                                            <span>Home Three</span>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-12">
                                    <a href="index-4.html">
                                        <img style="width: 100%" src="assets/images/menu_img/home_4.jpg" alt="image">
                                        <span>Home Four</span>
                                    </a>
                                </div>
                            </div>
                        </div> -->
                    </li>
                    <li><a href="https://cryptogemx.com/#trading_bot" class="scrolls">Trading Bot</a></li>
                    <li><a href="https://cryptogemx.com/#how_work" class="scrolls">How it work</a></li>
                    <li><a href="https://cryptogemx.com/#team" class="scrolls">Team</a></li>
                    <li><a href="pricing-plans.php">Plans</a></li>
                    <li><a href="contact.php" class="scrolls">Contact</a></li>
                </ul>

            </div>


        </nav>
    </div><!-- .container END -->
</header>    <!-- header section end -->
