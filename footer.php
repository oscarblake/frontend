    <!-- footer section start -->
    <footer class="xs-footer-sec xs-footer-v3" id="blog">
    <div class="container">
        <div class="footer-area">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="footer-widget">
                        <div class="footer-logo">
                            <a href="https://cryptogemx.com/">
                                <img src="assets/images/trypto_logo_green.png" alt="">
                            </a>
                        </div>
                        <p>
                            A better way to present your money using fully featured digital currency and now available
                            on
                        </p>
                    </div>
                </div><!-- col end -->
                <div class="col-lg-2 offset-lg-1 col-sm-6">
                    <div class="footer-widget">
                        <h4 class="widget-title">Company</h4>
                        <ul>
                            <li><a href="#home-top" class="scrolls"> About us</a></li>
                            <li><a href="#how_work" class="scrolls">How it work</a></li>
                            <li><a href="#team" class="scrolls">Team</a></li>
                            
                        </ul>
                    </div>
                </div><!-- col end -->
                <div class="col-lg-2 col-sm-6">
                    <div class="footer-widget support">
                        <h4 class="widget-title">Others</h4>
                        <ul>
                            <li><a href="/contact.php" class="scrolls">Contact</a></li>
                            <li><a href="pricing-plans.php"> Packages Plans</a></li>
                        </ul>
                    </div>
                </div><!-- col end -->
                <div class="col-lg-3 offset-lg-1 col-sm-6">
                    <div class="footer-widget">
                        <h4 class="widget-title">Contact</h4>
                        <form  method="get" class="widget-subscibe">
                            <input type="email" name="email" class="subscribe-email" placeholder="Email">
                            <button type="submit" class="subs-btn"><i class="fa fa-paper-plane"></i> </button>
                        </form>
                        <ul>
                            <li>Tel: +447555647840</li>
                            <li>Email: support@cryptogemx.com</li>
                        </ul>
                    </div>
                </div><!-- col end -->
            </div><!-- row end -->
        </div><!-- footer area end -->

        <!-- copyright area -->
        <div class="footer-copyright">
            <p>
                Copyright © 2020 cryptosky
            </p>
        </div>

    </div><!-- container end -->
</footer>

<!--back to top start-->
<div class="BackTo">
    <a href="#" class="fa fa-angle-up" aria-hidden="true"></a>
</div>
<!--back to top end-->

<script src="assets/js/jquery-3.2.1.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/navigation.js"></script>
<script src="assets/js/jquery.appear.min.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/chart.min.js"></script>
<script src="assets/js/particles.min.js"></script>
<script src="assets/js/smooth-scroling.js"></script>
<script src="assets/js/main.js"></script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script src='https://cdn.jsdelivr.net/gsap/1.19.1/TweenMax.min.js'></script>
<script src="assets/js/script.js"></script>
</body>
</html>
    <!-- footer section end -->
