<?php
include 'header.php'
?>

<!-- banner start -->
<section class="banner-sec banner-v2 banner-v3" id="home-top">
   <div class="banner-item">
        <div class="banner-table">
           <div class="banner-table-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="banner-content">
                                <h1 class="banner-title">All-in-one trading platform for your Bitcoins and other Cryptocurrency</h1>
                                <p>
                                    Trading, Portfolio, Arbitrage, Signals, Demo, Bots - everything you need to easily manage crypto assets, within one interface.
                                </p>
                                <a href="https://client.cryptogemx.com/register" class="btn btn-v3">Get Started Free</a>
                            </div>
                        </div>
                    </div>
                </div><!--  .container END -->
           </div>
        </div>
        <div class="banner-v2-img">
            <img class="banner-img" src="assets/images/banner/cryptoskytheme13.jpg" alt="">
            <img class="banner-ico banner-ico-1" src="assets/images/banner/bitcoin.png" alt="">
        </div>
   </div>

</section>    <!-- banner section end -->


<!-- blockcain section start -->
<section class="blockcain-top-sec blockcain-v3">
  <div class="container">
      <div class="row">
          <div class="col-md-6 wow fadeInUp" data-wow-duration="1.5s">
              <div class="blockcain-top">
                  <h2 class="hidden-title">Crypto</h2>
                  <h3 class="column-title">Never miss an opportunity with Cryptocurrency trading signals</h3>
              </div>
          </div><!-- col end -->
          <div class="col-md-5 offset-md-1 wow fadeInUp" data-wow-duration="2s">
              <div class="blockcain-top-content">
              <h3 class="hidden-title"><?php  echo date("Y")?></h3>
                  <p>
                  Our algorithms analyze over 10,000 crypto pairs every second to detect the best opportunities with a strongest uptrend potential.
                  </p>
              </div>
          </div>
      </div>
  </div>
</section>    <!-- blockcain section end -->

    <!-- docuemnt section start -->
<section class="docement-v3 section-p-120" id="trading_bot">
  <div class="container">
     <div class="row">
          <div class="col-lg-12 mx-auto">
              <div class="section-title-item v3">
                  <!-- <small class="xs-section-title">Document</small> -->
                  <h2 class="section-title">Discover best opportunities to maximize your profit</h2>
              </div>
          </div>
      </div><!-- row end -->
      <div class="row">
        <div class="col-md-6">
            <style>
                .nav {
                    display: block;
                }
            </style>
            <div class="nav xs-tabs-menu" id="v-pills-tab" role="tablist"
                 aria-orientation="horizontal">
                 <a class="active col-md" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab"
                    aria-controls="v-pills-home" aria-selected="true">
                        TRADING BOT
                        <p>
                            Get an advantage over the fluctuating market with minimum risks with our automated algorithms generating a profit on every market move.
                        </p>
                </a>


                    <a class="col-md" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-paper" role="tab"
                    aria-controls="v-pills-paper" aria-selected="false">
                    SIGNALS
                    <p style="color: #e0e4e3">
                        Analyzing over 10,000 crypto pairs every second, our algorithms automatically detect the coins with the strongest potential.
                    </p>
                    </a>


                    <a class="col-md" id="v-pills-report-tab" data-toggle="pill" href="#v-pills-reports" role="tab"
                    aria-controls="v-pills-reports" aria-selected="false">
                    ARBITRAGE
                    <p style="color: #e0e4e3">
                       Make profit on a price difference buying crypto on an exchange where the price is lower and then selling it on an exchange where the price is higher.
                    </p>
                    </a>
            </div><!-- tab menu end -->
        </div>
        <div class="col-md-6">
            <div class="road-map-tab">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane tab_item_1 show active" id="v-pills-home" role="tabpanel"
                         aria-labelledby="v-pills-home-tab">
                        <div class="row">
                            <div class="slider-wrap">
                              <div id="card-slider" class="slider">

                                <div class="slider-item">
                                    <div class="row">
                                        <div class="col-md-2 p-0">
                                            <img class="logoex" src="assets/images/1.jpg">
                                        </div>
                                        <div class="col-md-7">
                                            <span class="coinc">TOMO/USDT</span><br>
                                            <span class="extitle">HitBTC</span><br>
                                            <span class="detected">Detected 18 hours ago</span>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="numberpercent">+7.02%</span><br>
                                            <span class="profit">PROFIT</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider-item">
                                    <div class="row">
                                        <div class="col-md-2 p-0">
                                            <img class="logoex" src="assets/images/2.png">
                                        </div>
                                        <div class="col-md-7">
                                            <span class="coinc">LEND/BTC</span><br>
                                            <span class="extitle">Bitfinex</span><br>
                                            <span class="detected">Detected 10 hours ago</span>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="numberpercent">+6.13%</span><br>
                                            <span class="profit">PROFIT</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider-item">
                                    <div class="row">
                                        <div class="col-md-2 p-0">
                                            <img class="logoex" src="assets/images/3.png">
                                        </div>
                                        <div class="col-md-7">
                                            <span class="coinc">DX/BTC</span><br>
                                            <span class="extitle">Binance</span><br>
                                            <span class="detected">Detected 11 hours ago</span>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="numberpercent">+5.06%</span><br>
                                            <span class="profit">PROFIT</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider-item">
                                    <div class="row">
                                        <div class="col-md-2 p-0">
                                            <img class="logoex" src="assets/images/4.jpg">
                                        </div>
                                        <div class="col-md-7">
                                            <span class="coinc">MAN/BTC</span><br>
                                            <span class="extitle">Kraken</span><br>
                                            <span class="detected">Detected 18 hours ago</span>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="numberpercent">+7.02%</span><br>
                                            <span class="profit">PROFIT</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider-item">
                                    <div class="row">
                                        <div class="col-md-2 p-0">
                                            <img class="logoex" src="assets/images/5.jpg">
                                        </div>
                                        <div class="col-md-7">
                                            <span class="coinc">EOS/USDT</span><br>
                                            <span class="extitle">Huobi Global</span><br>
                                            <span class="detected">Detected 5 hours ago</span>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="numberpercent">+4.12%</span><br>
                                            <span class="profit">PROFIT</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider-item">
                                    <div class="row">
                                        <div class="col-md-2 p-0">
                                            <img class="logoex" src="assets/images/1.jpg">
                                        </div>
                                        <div class="col-md-7">
                                            <span class="coinc">LXT/ETH</span><br>
                                            <span class="extitle">HitBTC</span><br>
                                            <span class="detected">Detected 20 hours ago</span>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="numberpercent">+7.09%</span><br>
                                            <span class="profit">PROFIT</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider-item">
                                    <div class="row">
                                        <div class="col-md-2 p-0">
                                            <img class="logoex" src="assets/images/2.png">
                                        </div>
                                        <div class="col-md-7">
                                            <span class="coinc">LBA/ETH</span><br>
                                            <span class="extitle">Bitfinex</span><br>
                                            <span class="detected">Detected 13 hours ago</span>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="numberpercent">+4.50%</span><br>
                                            <span class="profit">PROFIT</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider-item">
                                    <div class="row">
                                        <div class="col-md-2 p-0">
                                            <img class="logoex" src="assets/images/3.png">
                                        </div>
                                        <div class="col-md-7">
                                            <span class="coinc">BCD/USDT</span><br>
                                            <span class="extitle">Binance</span><br>
                                            <span class="detected">Detected 5 hours ago</span>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="numberpercent">+6.32%</span><br>
                                            <span class="profit">PROFIT</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider-item">
                                    <div class="row">
                                        <div class="col-md-2 p-0">
                                            <img class="logoex" src="assets/images/4.jpg">
                                        </div>
                                        <div class="col-md-7">
                                            <span class="coinc">OCN/ETH</span><br>
                                            <span class="extitle">Kraken</span><br>
                                            <span class="detected">Detected 8 hours ago</span>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="numberpercent">+5.78%</span><br>
                                            <span class="profit">PROFIT</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider-item">
                                    <div class="row">
                                        <div class="col-md-2 p-0">
                                            <img class="logoex" src="assets/images/5.jpg">
                                        </div>
                                        <div class="col-md-7">
                                            <span class="coinc">TROY/BNB</span><br>
                                            <span class="extitle">Huobi Global</span><br>
                                            <span class="detected">Detected 17 hours ago</span>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="numberpercent">+7.55%</span><br>
                                            <span class="profit">PROFIT</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            </div>
                        </div>
                    </div> <!-- tab pane tab_item_1-->
                    <div class="tab-pane tab_item_2" id="v-pills-paper" role="tabpanel"
                         aria-labelledby="v-pills-profile-tab">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="xs-white-tabs-content">

                                    <h3 class="xs-single-title">Why you will love our signals?</h3>
                                    <p>
                                        No need to be a PRO trader or turn trading into a full-time job
                                        Execute crypto signals right from the trading interface
                                        Set Stop Loss and Take Profit orders to minimize your risks
                                        Filter opportunities based on raise percentage, signal strength, exchange, and detection time
                                    </p>
                                    <a href="#" class="btn btn-v3">Join Now</a>
                                </div>
                            </div>
                        </div>
                    </div><!-- tab pane tab_item_2-->
                    <div class="tab-pane tab_item_3" id="v-pills-reports" role="tabpanel"
                         aria-labelledby="v-pills-report-tab">
                        <div class="report-table table-responsive">
                            <!-- TradingView Widget BEGIN -->
                            <div class="tradingview-widget-container">
                              <div class="tradingview-widget-container__widget"></div>
                              <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-screener.js" async>
                              {
                              "width": "100%",
                              "height": "400",
                              "defaultColumn": "overview",
                              "screener_type": "crypto_mkt",
                              "displayCurrency": "USD",
                              "colorTheme": "dark",
                              "locale": "en",
                              "isTransparent": false
                            }
                              </script>
                            </div>
                            <!-- TradingView Widget END -->
                        </div>
                    </div><!-- tab pane tab_item_3-->
                </div>
            </div>
        </div>
      </div><!-- row end -->
  </div><!-- container end -->
  <div class="documentaion-shap-img">
      <img class="d-shap-img-1 wow fadeInLeft" data-wow-duration="1.5s" src="assets/images/documentation/globe1.png" alt="">
      <img class="d-shap-img-2 wow fadeInRight" data-wow-duration="1.5s" src="assets/images/documentation/globe2.png" alt="">
      <img class="d-shap-img-3 wow fadeInUp" data-wow-duration="1.5s" src="assets/images/documentation/rectangle.png" alt="">
  </div>
</section>    <!-- document section end -->




<!-- blockcain business section start -->
<section class="blockcain-business-sec blockcain-business-v3" id="how_work">
  <div class="container">
      <div class="row">
          <div class="col-md-6 col-lg-6 wow fadeInUp" data-wow-duration="1.5s">
              <div class="blockcain-img">
                 <img src="assets/images/blockchain/block_img_2.png" alt="">
              </div>
          </div><!-- col end -->
          <div class="col-md-6 col-lg-5 offset-lg-1 wow fadeInUp" data-wow-duration="2s">
              <div class="blockcain-content">
                <small class="xs-section-title">Business solution</small>
                  <h3 class="column-title">How does it all work?</h3>
                  <p>
                        With thousands of crypto pairs, the CryptoSky Signal tool presents only the strongest opportunities.
                  </p>
                  <p>
                        <ul class="howitworks">
                            <li>- Our monitors crypto pairs 24/7 to detect anomalies</li>
                            <li>- Coins with the highest growth potential appear in the list</li>
                            <li>- Check the chart to see if you would like to use the signal</li>
                            <li>- Set a Smart order to minimize your risks</li>
                        </ul>
                  </p>
                  <a href="https://client.cryptogemx.com/register" class="btn btn-v3">Get A Quote</a>
              </div>
          </div>
      </div>
  </div>
</section>    <!-- blockcain business section end -->

<!-- client logo section start -->
<section class="client-logo-sec client-logo-v3">
  <div class="container">
     <div class="client-logo-item">
        <center><h3 style="margin-bottom: 60px">Crypto Signals are fully integrated with over 25 crypto exchanges</h3></center>

        <img style="width: 100%" src="assets/images/exchanges.png">
        <!-- <div class="row owl-carousel" id="client-slider">
            <div class="col-sm wow fadeInUp" data-wow-duration="1.5s">
                <div class="client-logo">
                    <img src="assets/images/client_logo/logo_style1.png" alt="">
                </div>
            </div>
            <div class="col-sm wow fadeInUp" data-wow-duration="2s">
                <div class="client-logo">
                    <img src="assets/images/client_logo/logo_style2.png" alt="">
                </div>
            </div>
            <div class="col-sm wow fadeInUp" data-wow-duration="2.5s">
                <div class="client-logo">
                    <img src="assets/images/client_logo/logo_style3.png" alt="">
                </div>
            </div>
            <div class="col-sm wow fadeInUp" data-wow-duration="3s">
                <div class="client-logo">
                    <img src="assets/images/client_logo/logo_style4.png" alt="">
                </div>
            </div>
            <div class="col-sm wow fadeInUp" data-wow-duration="3.5s">
                <div class="client-logo">
                    <img src="assets/images/client_logo/logo_style5.png" alt="">
                </div>
            </div>
        </div> -->
     </div>
  </div>
</section>    <!-- client logo section end -->

<!-- featured section start -->
<section class="featured-sec featured-v3 section-p-120"
style="background: url(assets/images/feature/blockcain_feature_bg_v3.jpg) no-repeat center center /cover">
  <div class="container" id="particles-js">
      <div class="row">
          <div class="col-lg-8 mx-auto">
              <div class="section-title-item v3">
                  <small class="xs-section-title">Main Features</small>
                  <h2 class="section-title">Discover best opportunities to maximize your profit</h2>
              </div>
          </div>
      </div><!-- row end -->
      <div class="main-fetured-item">
          <div class="row">
              <div class="col-md-4 wow fadeInUp" data-wow-duration="1.5s">
                  <div class="single-feaured-item">
                      <img src="assets/images/feature/icon-1.png" alt="">
                      <h3 class="feature-title">
                         1. SIGN UP
                      </h3>
                      <p>
                         Open a free client
                      </p>
                  </div>
              </div><!-- col end -->
              <div class="col-md-4 wow fadeInUp" data-wow-duration="2s">
                  <div class="single-feaured-item">
                      <img src="assets/images/feature/icon-2.png" alt="">
                      <h3 class="feature-title">
                        2. Connect exchanges
                      </h3>
                      <p>
                        Your funds will remain on the exchange balance and we won’t have access to them.
                      </p>
                  </div>
              </div><!-- col end -->
              <div class="col-md-4 wow fadeInUp" data-wow-duration="2.5s">
                  <div class="single-feaured-item">
                      <img src="assets/images/feature/icon-3.png" alt="">
                      <h3 class="feature-title">
                         3. Start trading cryptocurrency
                      </h3>
                      <p>
                         All orders you perform through CryptoSky are secured with high-end 2048-bit encryption.
                      </p>
                  </div>
              </div><!-- col end -->
          </div><!-- row end -->
      </div><!-- main-fetured-item end -->

      <div class="featured-poligonal-img wow fadeInRight" data-wow-duration="1.5s">
         <img class="poligonal-img" src="assets/images/feature/poligonal_3.png" alt="">
      </div>

  </div>
</section>    <!-- featured section end -->






<!-- team section start -->
<section class="team-sec team-v2-sec team-v3 section-p-120" id="team">
  <div class="container">
     <div class="row">
        <div class="col-lg-8 mx-auto">
            <div class="section-title-item">
                <small class="xs-section-title">Our top experts for this year</small>
                <h2 class="section-title">Team Members</h2>
            </div>
        </div>
    </div><!-- row end -->
    <div class="row">
        <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-duration="1.5s">
            <div class="single-team">
               <div class="team-img">
                    <img src="assets/images/team/team_1.jpg" alt="">
               </div>
               <div class="team-content">
                   <h3 class="xs-title md">James Carter</h3>
                   <p>Data Scientist</p>
                   <div class="team-social">
                        <a href="#">
                            <i class="icon icon-facebook"></i>
                        </a>
                        <a href="#">
                            <i class="icon icon-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="icon icon-linkedin"></i>
                        </a>
                    </div>
               </div>
            </div>
        </div> <!-- Col End -->
        <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-duration="2s">
            <div class="single-team">
                <div class="team-img">
                    <img src="assets/images/team/team_2.jpg" alt="">
                </div>
                <div class="team-content">
                    <h3 class="xs-title md">Jenifar Lorany</h3>
                    <p>Marketing Head</p>
                    <div class="team-social">
                        <a href="#">
                            <i class="icon icon-facebook"></i>
                        </a>
                        <a href="#">
                            <i class="icon icon-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="icon icon-linkedin"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div> <!-- Col End -->
        <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-duration="2.5s">
            <div class="single-team">
                <div class="team-img">
                    <img src="assets/images/team/team_3.jpg" alt="">
                </div>
                <div class="team-content">
                    <h3 class="xs-title md">Michel Ramos</h3>
                    <p>Blockchain Developer</p>
                    <div class="team-social">
                        <a href="#">
                            <i class="icon icon-facebook"></i>
                        </a>
                        <a href="#">
                            <i class="icon icon-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="icon icon-linkedin"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div> <!-- Col End -->
        <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-duration="3s">
            <div class="single-team">
                <div class="team-img">
                    <img src="assets/images/team/team_4.jpg" alt="">
                </div>
                <div class="team-content">
                    <h3 class="xs-title md">Lorence Katie</h3>
                    <p>Data Analyst</p>
                    <div class="team-social">
                        <a href="#">
                            <i class="icon icon-facebook"></i>
                        </a>
                        <a href="#">
                            <i class="icon icon-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="icon icon-linkedin"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div> <!-- Col End -->
   </div> <!-- Row End -->

  </div>
</section>    <!-- team section end -->
<!-- roadmap section start -->
<section class="roadmap-sec roadmap-v3 section-p-120" style="">
  <div class="container-fluid">
     <div class="row">
          <div class="col-lg-8 mx-auto">
              <div class="section-title-item">
                  <small class="xs-section-title">Process</small>
                  <h2 class="section-title">Company Roadmap</h2>
              </div>
          </div>
      </div><!-- row end -->
      <div class="roadmap-timeline">
          <img src="assets/images/roadmap/timelinered.png" alt="">
          <div class="custom-container container">
          <div class="row roadmap-timeline-item">
                  <div class="col-md xs-roadmap wow fadeInUp" data-wow-duration="1.5s">
                      <div class="single-roadmap-timeline">
                          <b></b>
                          <h3>April 2015</h3>
                          <p>
                              Creation of Our
                              Company
                          </p>

                      </div>
                  </div>

                  <div class="col-md xs-roadmap wow fadeInUp" data-wow-duration="2s">
                      <div class="single-roadmap-timeline">
                          <b></b>
                          <h3>April 2016</h3>
                          <p>
                          Reach $20 Million Milestone
                          </p>

                      </div>
                  </div>

                  <div class="col-md xs-roadmap wow fadeInUp" data-wow-duration="2.5s">
                      <div class="single-roadmap-timeline">
                            <b></b>
                          <h3>March 2017</h3>
                          <p>
                             Best ICO Brand
                          </p>

                      </div>
                  </div>

                  <div class="col-md xs-roadmap wow fadeInUp" data-wow-duration="3s">
                      <div class="single-roadmap-timeline">
                            <b></b>
                          <h3>May 2017</h3>
                          <p>
                          Partnership with Huobi
                          </p>

                      </div>
                  </div>

                  <div class="col-md xs-roadmap wow fadeInUp" data-wow-duration="3.5s">
                      <div class="single-roadmap-timeline">
                            <b></b>
                          <h3>June 2018</h3>
                          <p>
                          Invest in Bitcoin & Lisk
                          </p>

                      </div>
                  </div>

                                    <div class="col-md xs-roadmap wow fadeInUp" data-wow-duration="3.5s">
                      <div class="single-roadmap-timeline">
                            <b></b>
                          <h3>Aug 2019</h3>
                          <p>
                                +80.000 Clients Using our Services
                          </p>

                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div><!-- container fluid end -->

</section>    <!-- roadmap section end -->


    <!-- social list section start -->
    <section class="social-list-sec social-list-v3 section-p-120"
         style="">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="section-title-item">
                    <small class="xs-section-title">On Social</small>
                    <h2 class="section-title">Join us on Socials</h2>
                </div>
            </div>
        </div>
        <div class="row">
           <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration=".5s">
               <div class="single-social-list facebook">
                   <i class="fa fa-facebook"></i>
                   <h3 class="xs-title sm">Facebook</h3>
                   <a href="#">
                       <span class="icon icon-arrow-right"></span>
                   </a>
               </div>
           </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="1s">
                <div class="single-social-list twitter">
                    <i class="fa fa-twitter"></i>
                    <h3 class="xs-title sm">Twitter</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="1.5s">
                <div class="single-social-list github">
                    <i class="fa fa-github-alt"></i>
                    <h3 class="xs-title sm">Github</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="2s">
                <div class="single-social-list slack">
                    <i class="fa fa-slack"></i>
                    <h3 class="xs-title sm">Slack</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="2.5s">
                <div class="single-social-list bitcoin">
                    <i class="fa fa-btc"></i>
                    <h3 class="xs-title sm">BitTalk</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="3s">
                <div class="single-social-list youtube">
                    <i class="fa fa-youtube-play"></i>
                    <h3 class="xs-title sm">Youtube</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="3.5s">
                <div class="single-social-list medium">
                    <i class="fa fa-medium"></i>
                    <h3 class="xs-title sm">Medium</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->

        </div>

    </div>
</section>    <!-- social list section end -->



<?php
include 'footer.php'
?>