<?php 
include('header.php')
?>


<!-- roadmap section start -->
<section class="roadmap-sec roadmap-v3 section-p-120" style="">
  <div class="container-fluid">
     <div class="row">
          <div class="col-lg-8 mx-auto">
              <div class="section-title-item">
                  <h2 class="section-title">Contact us</h2>
              </div>
          </div>
      </div><!-- row end -->
      <div class="roadmap-timeline">

            <div class="container">
                <div class="row">

                <!--Grid column-->
                <div class="col-md-9 mb-md-0 mb-5">
                    <form id="contact-form" name="contact-form" action="#" method="POST">

                        <!--Grid row-->
                        <div class="row">

                            <!--Grid column-->
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <input type="text" id="name" name="name" class="form-control">
                                    <label for="name" class="">Your name</label>
                                </div>
                            </div>
                            <!--Grid column-->

                            <!--Grid column-->
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <input type="text" id="email" name="email" class="form-control">
                                    <label for="email" class="">Your email</label>
                                </div>
                            </div>
                            <!--Grid column-->

                        </div>
                        <!--Grid row-->

                        <!--Grid row-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="md-form mb-0">
                                    <input type="text" id="subject" name="subject" class="form-control">
                                    <label for="subject" class="">Subject</label>
                                </div>
                            </div>
                        </div>
                        <!--Grid row-->

                        <!--Grid row-->
                        <div class="row">

                            <!--Grid column-->
                            <div class="col-md-12">

                                <div class="md-form">
                                    <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea"></textarea>
                                    <label for="message">Your message</label>
                                </div>

                            </div>
                        </div>
                        <!--Grid row-->

                    </form>

                    <div class="text-center text-md-left">
                        <a class="btn btn-primary" onclick="document.getElementById('contact-form').submit();">Send</a>
                    </div>
                    <div class="status"></div>
                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-md-3 text-center">
     
                </div>
                <!--Grid column-->

            </div>
            </div>
      </div>
  </div><!-- container fluid end -->

</section>    <!-- roadmap section end -->


    <!-- social list section start -->
    <section class="social-list-sec social-list-v3 section-p-120"
         style="">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="section-title-item">
                    <small class="xs-section-title">On Social</small>
                    <h2 class="section-title">Join us on Socials</h2>
                </div>
            </div>
        </div>
        <div class="row">
           <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration=".5s">
               <div class="single-social-list facebook">
                   <i class="fa fa-facebook"></i>
                   <h3 class="xs-title sm">Facebook</h3>
                   <a href="#">
                       <span class="icon icon-arrow-right"></span>
                   </a>
               </div>
           </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="1s">
                <div class="single-social-list twitter">
                    <i class="fa fa-twitter"></i>
                    <h3 class="xs-title sm">Twitter</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="1.5s">
                <div class="single-social-list github">
                    <i class="fa fa-github-alt"></i>
                    <h3 class="xs-title sm">Github</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="2s">
                <div class="single-social-list slack">
                    <i class="fa fa-slack"></i>
                    <h3 class="xs-title sm">Slack</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="2.5s">
                <div class="single-social-list bitcoin">
                    <i class="fa fa-btc"></i>
                    <h3 class="xs-title sm">BitTalk</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="3s">
                <div class="single-social-list youtube">
                    <i class="fa fa-youtube-play"></i>
                    <h3 class="xs-title sm">Youtube</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="3.5s">
                <div class="single-social-list medium">
                    <i class="fa fa-medium"></i>
                    <h3 class="xs-title sm">Medium</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->

        </div>

    </div>
</section>    <!-- social list section end -->
<style>
    label {
    color: white;
}
ul p {
    color: white;
}
.list-unstyled {
    color: white;
}
.btn-primary {
    background-image: linear-gradient(to right, #02464a 0%, #056971 51%, #3967f5 100%);
}
</style>    

<?php 
include('footer.php')
?>
