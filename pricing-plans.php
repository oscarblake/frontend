<?php 
include('header.php')
?>


<!-- roadmap section start -->
<section class="roadmap-sec roadmap-v3 section-p-120" style="">
  <div class="container-fluid">
     <div class="row">
          <div class="col-lg-8 mx-auto">
              <div class="section-title-item">
                  <h2 class="section-title">Pricing Plans</h2>
              </div>
          </div>
      </div><!-- row end -->
      <div class="roadmap-timeline">

            <div class="container">
                    <div class="pricing-area">
    <div class="pricing-table">
      <div class="pricing-title">Standard</div>
      <div class="price">€ 250 - 10 000</div>
      <div class="pricing-type">DAILY PROFIT</div>
      <div class="pricing-details">
        <ul style="text-align: center;">
          <li>0.6 - 1 %</li>
          <li>Maximum 5 exchanges<br>connected </li>
        </ul>
      </div>
      <a href="https://client.cryptogemx.com/"> <div class="btn-plan">Choose Plan</div></a>
    </div>
    <div class="pricing-table">
      <div class="pricing-title">Silver</div>
      <div class="price">€10 000 - 50 000</div>
      <div class="pricing-type">DAILY PROFIT</div>
      <div class="pricing-details">
        <ul style="text-align: center;">
          <li>1 - 1.5 %</li>
          <li>Maximum 10 exchanges<br> connected</li>
        </ul>
      </div>
      <a href="https://client.cryptogemx.com/"> <div class="btn-plan">Choose Plan</div></a>
    </div>
    <div class="pricing-table">
      <div class="pricing-title">Gold</div>
      <div class="price">€ 50000+</div>
      <div class="pricing-type">DAILY PROFIT</div>
      <div class="pricing-details">
        <ul style="text-align: center;">
          <li>1.5 - 2.5 %</li>
          <li>Maximum 25+ exchanges <br>connected</li>
        </ul>
      </div>
      <a href="https://client.cryptogemx.com/"> <div class="btn-plan">Choose Plan</div></a>
    </div>
  </div>
            </div>
      </div>
  </div><!-- container fluid end -->

</section>    <!-- roadmap section end -->


    <!-- social list section start -->
    <section class="social-list-sec social-list-v3 section-p-120"
         style="">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="section-title-item">
                    <small class="xs-section-title">On Social</small>
                    <h2 class="section-title">Join us on Socials</h2>
                </div>
            </div>
        </div>
        <div class="row">
           <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration=".5s">
               <div class="single-social-list facebook">
                   <i class="fa fa-facebook"></i>
                   <h3 class="xs-title sm">Facebook</h3>
                   <a href="#">
                       <span class="icon icon-arrow-right"></span>
                   </a>
               </div>
           </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="1s">
                <div class="single-social-list twitter">
                    <i class="fa fa-twitter"></i>
                    <h3 class="xs-title sm">Twitter</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="1.5s">
                <div class="single-social-list github">
                    <i class="fa fa-github-alt"></i>
                    <h3 class="xs-title sm">Github</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="2s">
                <div class="single-social-list slack">
                    <i class="fa fa-slack"></i>
                    <h3 class="xs-title sm">Slack</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="2.5s">
                <div class="single-social-list bitcoin">
                    <i class="fa fa-btc"></i>
                    <h3 class="xs-title sm">BitTalk</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="3s">
                <div class="single-social-list youtube">
                    <i class="fa fa-youtube-play"></i>
                    <h3 class="xs-title sm">Youtube</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->
            <div class="col-lg col-6 col-md-3 wow fadeInUp" data-wow-duration="3.5s">
                <div class="single-social-list medium">
                    <i class="fa fa-medium"></i>
                    <h3 class="xs-title sm">Medium</h3>
                    <a href="#">
                        <span class="icon icon-arrow-right"></span>
                    </a>
                </div>
            </div><!-- single-social-list-->

        </div>

    </div>
</section>    <!-- social list section end -->
<style>
    label {
    color: white;
}
ul p {
    color: white;
}
.list-unstyled {
    color: white;
}
.btn-primary {
    background-image: linear-gradient(to right, #02464a 0%, #056971 51%, #3967f5 100%);
}





.pricing-area {
  display: flex;
  justify-content: center;
}

.pricing-table {
    display: inline-block;
    padding: 35px 15px 35px 15px;
    margin: 15px;
    width: 275px;
    border-radius: 10px;
    box-shadow: 5px 5px 5px #038066;
    background-color: #038368;
    transition: .35s;
}

.pricing-table:hover {
    background-color: #07534a;
}

.pricing-title {
    text-align: center;
    font-family: "Avenir Next", Avenir, 'Helvetica Neue', 'Lato', 'Segoe UI', Helvetica, Arial, sans-serif;
    font-weight: 700;
    font-size: 30px;
    color: #ffffff;
    border-bottom: 3px solid #ffffff;
    padding-bottom: 15px;
}

.price {
    text-align: center;
    color: #ffffff;
    font-family: "Avenir Next", Avenir, 'Helvetica Neue', 'Lato', 'Segoe UI', Helvetica, Arial, sans-serif;
    font-size: 30px;
    padding: 30px 0 30px 0;
}

.pricing-type {
  color: #ffffff;
  text-align: center;
  font-family: "Avenir Next", Avenir, 'Helvetica Neue', 'Lato', 'Segoe UI', Helvetica, Arial, sans-serif;
  font-weight: 700;
}

.pricing-details {
  color: #ffffff;
  font-family: "Avenir Next", Avenir, 'Helvetica Neue', 'Lato', 'Segoe UI', Helvetica, Arial, sans-serif;
  font-size: 16px;
  line-height: 1.25;
  padding: 25px 0 25px 0;
  height: 160px;
}

.btn-plan {
    color: #ffffff;
    font-family: "Avenir Next", Avenir, 'Helvetica Neue', 'Lato', 'Segoe UI', Helvetica, Arial, sans-serif;
    text-align: center;
    font-weight: 700;
    background-color: #0e4144;
    padding: 12px 0 12px 0;
    margin: 0 20px 0 20px;
    border-radius: 5px;
    transition: .35s;
}

.btn-plan:hover {
  background-color: #038368;
  cursor: pointer;
  
}
</style>    

<?php 
include('footer.php')
?>
